# Documentação para API do Gestprev
## Versão atual

Atualmente a API possui apenas uma versão: v1. Por padrão, todos requests utilizam a versão v1:

    Accept: application/vnd.gestprev.v1+json

Você também pode forçar a versão da API através de um cabeçalho "Accept".

## Schema
Todo acesso à API é feito sobre HTTP e acessado do domínio `gestprevweb.com.br`. Todos os dados são recebidos e transmitidos como JSON.

    curl -iH 'Authorization: Token token="f0c8db582459500da7040045c2f18fb5"' http://gestprevweb.com.br/api/vinculos
    
    HTTP/1.1 200 OK
    X-Rate-Limit-Limit: 10000
    X-Rate-Limit-Remaining: 9999
    X-Rate-Limit-Reset: 1387552399
    Content-Type: application/json; charset=utf-8
    Vary: Accept-Encoding
    X-UA-Compatible: IE=Edge
    ETag: "93c8d5c683a0abae05e58a924ae859c6"
    Cache-Control: max-age=0, private, must-revalidate
    X-Request-Id: 0477980ba2423caa4152eca80caec237
    X-Runtime: 17.976294
    Transfer-Encoding: chunked
    
    []

## Autenticação
Todos os requests para a API devem ser autenticados utilizando um token.

O token é gerado através do sistema de gestão Gestprev.

A autenticação é feita através de um cabeçalho "Authorization:", informando o token como valor. 

> Authorization: Token token="5e86f986aabce5fcf9472be11e31cafc"

Exemplo com `curl`:

> curl -H 'Authorization: Token token="5e86f986aabce5fcf9472be11e31cafc"' gestprevweb.com.br/api/servidores/12356

Se o token for inválido, o sistema retornará status `401 Unauthorized`.

    curl -iH 'Authorization: Token token="token_invalido"' gestprevweb.com.br/api/servidores/12356

    HTTP/1.1 401 Unauthorized
    WWW-Authenticate: Token realm="Application"
    Content-Type: text/html; charset=utf-8
    Vary: Accept-Encoding
    X-UA-Compatible: IE=Edge
    Cache-Control: no-cache
    X-Request-Id: 492e8aaa32a4f73eafd5a80b9869be3a
    X-Runtime: 0.392192
    Transfer-Encoding: chunked

    HTTP Token: Access denied.

## Parâmetros
Muitos métodos da API aceitam parâmetros adicionais. Para requisições GET, qualquer parâmetro não especificado como um segmento do caminho pode ser passado como um parâmetro query string do HTTP:

> curl gestprevweb.com.br/api/servidores?q[nome]=João&page=10

## Paginação

As requisições que retornam vários itens serão paginadas para 100 itens por padrão. Você pode especificar mais páginas com o parâmetro `?page`. Para alguns recursos, você também pode definir o número de itens retornados usando o parâmetro `?per_page`. O limite máximo que pode ser utilizado no parâmetro `?per_page` é 500.

> curl gestprevweb.com.br/api/servidores?page=10&per_page=250

Note que a numeração das páginas começa com 1, e se o parâmetro `?page` não estiver presente, será retornado a primeira página.

#### Link header

A informação da paginação está incluída no cabeçalho [Link](http://tools.ietf.org/html/rfc5988). É importante seguir os valores deste cabeçalho em vez de construir suas próprias URLSs.

    Link: <http://gestprevweb.com.br/api/servidores?page=3&per_page=100>; rel="next",
          <http://gestprevweb.com.br/api/servidores?page=50&per_page=100>; rel="last"
*Quebra de linha incluída para facilitar a leitura.*

Os valores possíveis para o `rel` são:

```
next: Mostra a URL da próxima página de resultados.  

last: Mostra o valor da última página de resultados.

first:  Mostra o valor da primeira página de resultados.

prev: Mostra a URL da página anterior de resultados.
```

#### Total de resultados

Para requisições paginadas, é retornado um cabeçalho `X-Total-Count` com o número total de registros.

    X-Total-Count: 12902

## Limite de requisições

Cada cliente pode fazer 10000 requisições por hora.

Você pode checar o cabeçalho HTTP de resposta de qualquer requisição para ver o status atual do limite:

    curl http://gestprevweb.com.br/api/servidores

    HTTP/1.1 200 OK
    X-Rate-Limit-Limit: 10000
    X-Rate-Limit-Remaining: 3667
    X-Rate-Limit-Reset: 1387560911

Os cabeçalhos mostram tudo o que você precisa saber sobre o seu limite:

```
X-RateLimit-Limit:      O número máximo de requisições que você pode fazer por hora

X-RateLimit-Remaining:  O número de requisições restantes no período atual

X-RateLimit-Reset:      A hora em que a quantidade de requisições será resetada (UTC epoch seconds)
```

Se você precisar utilizar a hora em um formato diferente, qualquer linguagem de programação pode dar conta do recado.
Por exemplo, se você abrir o console do seu navegador, você pode facilmente conseguir a hora do reset como um objeto de data em JavaScript:

    new Date(1387560911 * 1000)
    // => Fri Dec 20 2013 15:35:11 GMT-0200 (BRST)

Assim que ultrapassar o limite você receberá uma resposta de erro:

    HTTP/1.1 429 CUSTOM
    Content-Type: application/json; charset=utf-8
    Vary: Accept-Encoding
    X-UA-Compatible: IE=Edge
    Cache-Control: no-cache
    X-Request-Id: dddd849eb10062fd67772d56c29e6c0e
    X-Runtime: 0.058390
    Transfer-Encoding: chunked

    {
        "message": "Limite de requisições excedido, tente novamente mais tarde"
    }

## Recursos disponíveis

### Fontes pagadoras

Buscando a lista de fontes pagadoras.

#### Requisição
Segue o exemplo da URL que retorna a lista de fontes pagadoras com seus respectivos códigos:

> GET gestprevweb.com.br/api/fontes_pagadoras

#### Retorno

    {
        "fontes_pagadoras": [
            {
                "id": 1,
                "nome": "Fonte pagadora 1",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/fontes_pagadoras/1/servidores"
                }
            },
            {
                "id": 2,
                "nome": "Fonte pagadora 2",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/fontes_pagadoras/2/servidores"
                }
            }
        ]
    }

### Servidores por fontes pagadora

Buscando a lista de servidores por fonte pagadora.

#### Requisição  

Segue o exemplo da URL que retorna a lista de servidores pelo `id` da fonte pagadora informado:

> GET gestprevweb.com.br/api/fontes_pagadoras/1/servidores

#### Retorno

    {
        "servidores": [
            {
                "id": 1,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/1"
                }
            },
            {
                "id": 2,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/2"
                }
            },
            {
                "id": 3,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/3"
                }
            }
        ]
    }

O atributo `"servidor"` de `"links"`, mostra o endereço para buscar as informações do servidor.

### Categorias

Buscando a lista de categorias.

#### Requisição
Segue o exemplo da URL que retorna a lista de categorias com seus respectivos códigos:

> GET gestprevweb.com.br/api/categorias

#### Retorno

    {
        "categorias": [
            {
                "id": 1,
                "nome": "Categoria 1",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/categorias/1/servidores"
                }
            },
            {
                "id": 2,
                "nome": "Categoria 2",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/categorias/2/servidores"
                }
            }
        ]
    }

### Servidores por categoria

Buscando a lista de servidores por categoria.

#### Requisição  

Segue o exemplo da URL que retorna a lista de servidores pelo id da categoria informado:

> GET gestprevweb.com.br/api/categorias/1/servidores

#### Retorno

    {
        "servidores": [
            {
                "id": 1,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/1"
                }
            },
            {
                "id": 2,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/2"
                }
            },
            {
                "id": 3,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/3"
                }
            }
        ]
    }

### Vínculos

Buscando a lista de vínculos.

#### Requisição
Segue o exemplo da URL que retorna a lista de vínculos com seus respectivos códigos:

> GET gestprevweb.com.br/api/vinculos

#### Retorno

    {
        "vinculos": [
            {
                "id": 1,
                "nome": "Vínculo 1",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/vinculos/1/servidores"
                }
            },
            {
                "id": 2,
                "nome": "Vínculo 2",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/vinculos/2/servidores"
                }
            },
            {
                "id": 3,
                "nome": "Vínculo 3",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/vinculos/3/servidores"
                }
            }
        ]
    }

### Servidores por vínculo

Buscando a lista de servidores por vínculo.

#### Requisição  

Segue o exemplo da URL que retorna a lista de servidores pelo id do vínculo informado:

> GET gestprevweb.com.br/api/vinculos/1/servidores

#### Retorno

    {
        "servidores": [
            {
                "id": 1,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/1"
                }
            },
            {
                "id": 2,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/2"
                }
            },
            {
                "id": 3,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/3"
                }
            }
        ]
    }

### Situações

Buscando a lista de situações.

#### Requisição
Segue o exemplo da URL que retorna a lista de situações com seus respectivos códigos:

> GET gestprevweb.com.br/api/situacoes

#### Retorno

    {
        "situacoes": [
            {
                "id": 1,
                "nome": "Situação 1",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/situacoes/1/servidores"
                }
            },
            {
                "id": 2,
                "nome": "Situação 2",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/situacoes/2/servidores"
                }
            },
            {
                "id": 3,
                "nome": "Situação 3",
                "links": {
                    "servidores": "http://gestprevweb.com.br/api/situacoes/3/servidores"
                }
            }
        ]
    }

### Servidores por situação

Buscando a lista de servidores por situação.

#### Requisição  

Segue o exemplo da URL que retorna a lista de servidores pelo id da situação informado:

> GET gestprevweb.com.br/api/situacoes/1/servidores

#### Retorno

    {
        "servidores": [
            {
                "id": 1,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/1"
                }
            },
            {
                "id": 2,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/2"
                }
            },
            {
                "id": 3,
                "links": {
                    "servidor": "http://gestprevweb.com.br/api/servidores/3"
                }
            }
        ]
    }

### Servidor

Buscando as informações de um servidor

#### Requisição
Segue o exemplo da URL que retorna as informações do servidor buscado:

> GET gestprevweb.com.br/api/servidores/123

#### Retorno

    {
      "id": 123,
      "nome": "Nome do servidor",
      "rg": "123454321",
      "cpf": "123.456.789-10",
      "matricula": "1234",
      "nascimento": "1984-11-27",
      "email": "emaildo@servidor.com",
      "estadocivil": {
        "codigo": 1,
        "descricao": "Casado"
      },
      "sexo": {
        "codigo": 0,
        "descricao": "Masculino"
      },
      "cep": "12.345-678",
      "tipo_sanguineo": {
        "codigo": 7,
        "descricao": "O+"
      },
      "logradouro": "Rua do servidor",
      "numero_logradouro": "123",
      "bairro": "Bairro",
      "complemento": "Complemento informado",
      "vivo": true,
      "falecimento": "",
      "nome_pai": "Nome do pai do servidor",
      "nome_mae": "Nome da mãe do servidor",
      "nacionalidade": "BRASILEIRA",
      "pis_pasep": "12312345678",
      "carteira_trabalho": "0000012345",
      "uf_carteira": "SP",
      "local_carteira": "Cidade do servidor - SP",
      "data_emissao": "2007-04-12",
      "titulo_eleitor": "00123456781231",
      "zona": 123,
      "secao": 12,
      "telefone": "(12) 1234-1234",
      "telefone_comercial": "(12) 4321-1234",
      "celular": "(12) 91234-5432",
      "funcao": "0001",
      "funcao_original": "Função original do servidor",
      "lotacao": "Lotação do servidor",
      "invalido": false,
      "rg_data": "2001-12-12",
      "rg_emissor": "SSPSP",
      "cidade": "Cidade do servidor",
      "estado": "SP",
      "possui_aposentadoria": true,
      "grau_instrucao": {
        "codigo": "7",
        "descricao": "2º grau completo"
      },
      "curso": "",
      "cidade_naturalidade": "Cidade de naturalidade do servidor",
      "fonte_pagadora": "Fonte pagadora do servidor",
      "secretaria": "Geral",
      "vinculo": {
        "nome": "Vínculo do servidor",
        "codigo": "1",
        "grupo": "2"
      },
      "situacao": {
        "nome": "Situação do servidor",
        "codigo": "1",
        "grupo": ""
      },
      "categoria": {
        "nome": "Categoria do servidor",
        "codigo": "012343211",
        "grupo": "2"
      },
      "atualizacoes_cadastrais": [
        {
          "1234": "http://gestprevweb.com.br/api/atualizacoes_cadastrais/1234"
        },
        {
          "4321": "http://gestprevweb.com.br/api/atualizacoes_cadastrais/4321"
        }
      ],
      "dependentes":[
        {
          "17384":"http://gestprevweb.com.br/api/dependentes/17384"
        }
      ],
      "contribuicoes_anteriores":[
        {
          "id":1234,
          "data_inicio":"1985-09-02T00:00:00-03:00",
          "data_fim":"1988-06-16T00:00:00-03:00",
          "professor":false,
          "nome":"Empresa e Cia. ltda.",
          "dias":"",
          "fundamentacao":"",
          "orgao":"",
          "protocolo":"",
          "cnpj":"",
          "regime":"",
          "certidao":"",
          "cargo":"Ajudante de produção",
          "tipo":0
        }
    }

#### Observações

Em `contribuicoes_anteriores`, o campo `tipo` pode retornar 0 ou 1, representando iniciativa privada e serviço público, respectivamente.

### Lista de emails dos servidores

Mostra uma lista com nome e emails de servidores que possuem emails cadastrados.

A lista retornada é paginada e contem os headers de paginação.

Você pode aplicar os filtros de pesquisa a serem usados para selecionar os servidores que serão retornados.

#### Requisição
Segue o exemplo da URL que retorna os emails cadastrados dos servidores:

> GET gestprevweb.com.br/api/servidores/emails

#### Retorno

	{
	  "servidores": [
	    {
	      "nome": "Daniel da Cunha",
	      "email": "danieldc@exemplo.com"
	    },
	    {
	      "nome": "Douglas Ferreira Castro",
	      "email": "fcdouglas@exemplo.com"
	    },
	    {
	      "nome": "Doug Funny",
	      "email": "dougfunny@exemplo.com"
	    },
	    ...
	  ]
	}

### Cargos de um servidor

Buscando a lista de cargos de um servidor

#### Requisição
Segue o exemplo da URL que retorna a lista de cargos do servidor informado:

> GET gestprevweb.com.br/api/servidores/123/cargos

#### Retorno

    {
      "cargos" : [
        {
          "id" : 123,
          "magisterio" : true,
          "inicio" : "2014-10-10",
          "ultima_remuneracao_contribuicao" : {
            "valor" : "1234.56",
            "ano" : 2014,
            "mes" : 10
          },
          "servidor" : "http://gestprevweb.com.br/api/servidores/1234",
          "nivel" : "",
          "nome" : "Auxiliar administrativo",
          "fundamentacao" : "",
          "diretoria" : true,
          "atual" : true,
          "vencimento_base" : "1234.56",
          "fim" : "",
          "comissionado" : false
        }
      ]
    }

### Dependentes de um servidor

Buscando a lista de dependentes de um servidor

#### Requisição
Segue o exemplo da URL que retorna a lista de dependentes do servidor informado:

> GET gestprevweb.com.br/api/servidores/123/dependentes

#### Retorno

    {
      "dependentes":[
        {
          "servidor": "http://gestprevweb.com.br/api/servidores/6527",
            "id":17384,
            "nome":"Dependente 1",
            "rg":"",
            "cpf":"",
            "nascimento":"2003-08-17",
            "email":"",
            "estadocivil":{
              "codigo":0,
              "descricao":"Solteiro"
            },
            "sexo":{
              "codigo":1,
              "descricao":"Feminino"
            },
            "cep":"12.345-000",
            "tipo_sanguineo":{
              "codigo":3,
              "descricao":"A+"
            },
            "parentesco": {
              "codigo": 1,
              "descricao": "Filho(a)"
            },
            "logradouro":"Rua Exemplo, 123",
            "numero_logradouro":"",
            "bairro":"Bela Vista",
            "complemento":"Casa 01",
            "vivo":true,
            "falecimento":"",
            "nome_pai":"Pai do Dependente 1",
            "nome_mae":"Mãe do Dependente 1",
            "nacionalidade":"Brasileira",
            "data_emissao":"",
            "titulo_eleitor":"",
            "zona":"",
            "secao":"",
            "telefone":"",
            "telefone_comercial":"",
            "celular":"",
            "invalido":"",
            "rg_data":"",
            "rg_emissor":"",
            "cidade":"",
            "estado":"",
            "grau_instrucao":{
              "codigo":"",
              "descricao":"Não informado"
            },
            "curso":"",
            "cidade_naturalidade":""
          }
          ...
        ]
    }

### Dependente

Buscando as informações de um dependente

#### Requisição
Segue o exemplo da URL que retorna as informações do dependente buscado:

> GET gestprevweb.com.br/api/dependentes/1234

#### Retorno

    {
    	"servidor": "http://gestprevweb.com.br/api/servidores/6527",
        "id":1234,
        "nome":"Dependente buscado",
        "rg":"125467896",
        "cpf":"25485695862",
        "nascimento":"1999-03-15",
        "email":"",
        "estadocivil":{
          "codigo":0,
          "descricao":"Solteiro"
        },
        "sexo":{
          "codigo":1,
          "descricao":"Feminino"
        },
        "cep":"12.345-678",
        "tipo_sanguineo":{
          "codigo":7,
          "descricao":"O+"
        },
        "parentesco": {
          "codigo": 1,
          "descricao": "Filho(a)"
        },
        "logradouro":"Logradouro informado",
        "numero_logradouro":"123",
        "bairro":"Bairro informado",
        "complemento":"",
        "vivo":true,
        "falecimento":"",
        "nome_pai":"Nome do pai do dependente buscado",
        "nome_mae":"Nome da mãe do dependente buscado",
        "nacionalidade":"brasileira",
        "data_emissao":"",
        "titulo_eleitor":"",
        "zona":"",
        "secao":"",
        "telefone":"(12) 3456-7890",
        "telefone_comercial":"",
        "celular":"(12) 12345-1234",
        "invalido":false,
        "rg_data":"",
        "rg_emissor":"",
        "cidade":"Mogi das Cruzes",
        "estado":"SP",
        "grau_instrucao":{
          "codigo":"5",
          "descricao":"1º grau completo"
        },
        "curso":"",
        "cidade_naturalidade":"Mogi das Cruzes"
    }

### Lista de emails dos dependentes

Mostra uma lista com nome e emails de dependentes que possuem emails cadastrados.

A lista retornada é paginada e contem os headers de paginação.

Você pode aplicar os filtros de pesquisa a serem usados para selecionar os dependentes que serão retornados retornados.

#### Requisição
Segue o exemplo da URL que retorna os emails cadastrados dos dependentes:

> GET gestprevweb.com.br/api/dependentes/emails

#### Retorno

	{
	  "dependentes": [
	    {
	      "servidor": "http://gestprevweb.com.br/api/servidores/1234",
	      "nome": "João Emanuel da Cunha",
	      "email": "joaomanu@exemplo.com"
	    },
	    {
	      "servidor": "http://gestprevweb.com.br/api/servidores/1337",
	      "nome": "Joaquim Ferreira Castro",
	      "email": "jfcastro@exemplo.com"
	    },
	    {
	      "servidor": "http://gestprevweb.com.br/api/servidores/1444",
	      "nome": "José Hernandes",
	      "email": "hernandesjose@exemplo.com"
	    },
	    ...
	  ]
	}

### Atualizações cadastrais

Mostra as informações de uma atualização cadastral.

#### Requisição
Segue o exemplo da URL que retorna as informações da atualização cadastral:

> GET gestprevweb.com.br/api/atualizacoes_cadastrais/1234

#### Retorno
	
    {
      "servidor": "http://gestprevweb.com.br/api/servidores/12462",
      "id": 1234,
      "etapa": 4,
      "inicio": "2013-12-20T10:55:28-02:00",
      "conclusao": "2013-12-20T12:17:45-02:00",
      "alteracoes": {
        "servidor": [
          {
            "data": "2013-12-20T10:58:17-02:00",
            "dados": [
              [
                "cidade_naturalidade_id",
                [
                  "Não informado",
                  "Acrelândia"
                ]
              ],
              [
                "grau_instrucao",
                [
                  "Não informado",
                  "1º grau incompleto"
                ]
              ],
              [
                "telefone",
                [
                  "01177825200",
                  "(11) 97246-0000"
                ]
              ],
              [
                "celular",
                [
                  "Não informado",
                  "(11) 97246-0000"
                ]
              ]
              ...
          ]
          }
        ],
        "dependentes": [
          {
            "data": "2013-12-20T11:20:56-02:00",
            "nome": "RAFAEL LIMA",
            "dados": "Criou o registro"
          },
          {
            "data": "2013-12-20T11:23:31-02:00",
            "nome": "CRISTINA RIBEIRO",
            "dados": "Criou o registro"
          },
          {
            "data": "2013-12-20T12:08:48-02:00",
            "nome": "FERNANDES CUNHA",
            "dados": "Criou o registro"
          }
        ],
        "contribuicoes_anteriores": [
          {
            "data": "2013-12-20T12:10:28-02:00",
            "nome": "ACME LTD",
            "dados": "Criou o registro"
          },
          {
            "data": "2013-12-20T12:12:26-02:00",
            "nome": "CTBC",
            "dados": "Criou o registro"
          }
        ]
      }
    }
    

#### Observações

A chave `dados` contém os campos com seus valores de antes e depois da mudanças, no seguinte formato.

    "nome do campo",
    [
      "valor anterior",
      "novo valor"
    ]

A `etapa` retornada pode ser:
	
	1: Informações do servidor
	2: Informações dos dependentes
	3: Tempos de contribuição anterior
	4: Atualização concluída

Caso a atualização não tenha sido concluída ainda, o campo `conclusao` retornará `""`.

## Busca de recursos

### Busca de servidores usando critérios

Buscando uma lista de servidores que satisfazem determinados critérios.

#### Campos utilizáveis para pesquisa

Os campos que podem ser utilizados na pesquisa são: `nome, rg, matricula, nascimento, cpf, estadocivil, sexo, vivo, vinculo_id, categoria_id, situacao_id, fonte_pagadora_id, possui_aposentadoria`.

As requisições devem conter o parâmetro `q[]` com o nome do atributo a ser pesquisado seguido de um predicado e o valor da pesquisa.

Você pode agrupar vários critérios usando uma query string:

> GET gestprevweb.com.br/api/servidores?q[nome_eq]=João&q[cpf_eq]=123.456.789-10

### Busca de atualizações usando critérios

Buscando uma lista de atualizações que satisfazem determinados critérios.

#### Campos utilizáveis para pesquisa

Os campos que podem ser utilizados na pesquisa são: `servidor_id, inicio, etapa, conclusao`.

As requisições devem conter o parâmetro `q[]` com o nome do atributo a ser pesquisado seguido de um predicado e o valor da pesquisa.

Você pode agrupar vários critérios usando uma query string:

> GET gestprevweb.com.br/api/atualizacoes_cadastrais?q[servidor_id_eq]=123&q[etapa_eq]=2

#### Campos de data

A busca pode ser utilizada em campos de data (`inicio` e `conclusao`), no entanto, este é um caso especial.

Os campos retornam valores `datetime` e este formato deve ser respeitado na hora da consulta, também considerando a `timezone`.

Então, para buscar as atualizações concluídas em determinada data, deve-se usar o seguinte formato (com os predicados `_gteq` e `_lteq`), que usará uma cláusula `between` no SQL:

> GET gestprevweb.com.br/api/atualizacoes_cadastrais?q[conclusao_gteq]=2013-11-29T00:00:00-02:00&q[conclusao_lteq]=2013-11-29T23:59:59-02:00

Note o formato da data (`AAAA-MM-DD`) e a hora de início (`T00:00:00-02:00`) e fim (`T23:59:59-02:00`) com timezone.

O mesmo se aplica para consultas utilizando o campo `inicio`.

### Utilização dos campos e predicados

##### **eq**

O predicado "eq" retorna todos os registros em que o valor do campo é exatamente igual ao informado:

> GET gestprevweb.com.br/api/servidores?q[nome_eq]=João

> SELECT * FROM servidores WHERE nome = 'João'

**Oposto: `not_eq`**

##### **matches**

O predicado "matches" retorna todos os registros em que o campo tem valor correspondente ao informado:

> GET gestprevweb.com.br/api/servidores?q[nome_matches]=João

> SELECT * FROM servidores WHERE nome LIKE 'João'

**Opposite: `does_not_match`**

##### **cont**

O predicado "cont" retorna todos os registros em que o campo contém o valor informado:

> GET gestprevweb.com.br/api/servidores?q[nome_cont]=João

> SELECT * FROM servidores  WHERE (nome LIKE '%João%')

**Oposto: `not_cont`**

##### **start**

O predicado "start" retorna todos os registros em que o valor do campo começa com o valor informado:

> GET gestprevweb.com.br/api/servidores?q[nome_start]=João

> SELECT * FROM servidores  WHERE (nome LIKE 'João%')

**Oposto: `not_start`**

##### **end**

O predicado "end" retorna todos os registros em que o valor do campo termina com o valor informado:

> GET gestprevweb.com.br/api/servidores?q[nome_end]=Pereira

> SELECT * FROM servidores  WHERE (nome LIKE '%Pereira')

**Oposto: `not_end`**

##### **true**

O predicado "true" retorna todos os registros em que o campo é `true`. O "1" é utilizado para confirmar que o valor do campo é verdadeiro.

> GET gestprevweb.com.br/api/servidores?q[vivo_true]=1

> SELECT * FROM servidores  WHERE (vivo = 'true')

**Oposto: `false`**

##### **present**

O predicado "present" retorna todos os registros em que o campo esta presente (`not null` e `not a blank string`).

> GET gestprevweb.com.br/api/servidores?q[nome_present]=1

> SELECT * FROM servidores  WHERE (nome IS NOT NULL AND nome != '')

**Oposto: `blank`**

##### **null**

o predicado "null" retorna todos os registros em que o campo tem valor nulo:

> GET gestprevweb.com.br/api/servidores?q[nome_null]=1

> SELECT * FROM servidores  WHERE nome IS NULL

**Oposto: `not_null`**

##### **in**

O predicado "in" retorna todos os registros em que o valor do campo está contido em uma lista informada:

> gestprevweb.com.br/api/servidores?q[vinculo_id_in][]=1&q[vinculo_id_in][]=2&q[vinculo_id_in][]=3

> SELECT * FROM servidores WHERE vinculo_id IN (1, 2, 3)

**Oposto: `not_in`**

##### **cont_any**

O predicado "cont_any" retorna todos os registros em que o campo contém qualquer dos valores informados:

> gestprevweb.com.br/api/servidores?q[nome_cont_any][]=Luís&q[nome_cont_any][]=João

> SELECT * FROM servidores  WHERE ((nome LIKE '%Luís%' OR nome LIKE '%João%'))

**Oposto: `not_cont_any`**
